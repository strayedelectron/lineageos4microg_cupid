#!/bin/bash

echo ">> [$(date)] Running userscript before.sh before breakfast"

# roll back all git repos to before a data
#repo forall -c 'git checkout `git rev-list -n1 --before="2024-08-11 00:00" HEAD`'

# repopicks for xiaomi sm8450 devices
/root/userscripts/repopicks.sh lineage
#/root/userscripts/xiaomi_repopicks.sh
test $? -eq 0 || echo "ERROR: Failure while executing repopicks!"

# in case some patches dont apply successfully
exit 0
