#!/bin/bash

echo ">> [$(date)] Running userscript begin.sh before everything else for $DEVICE_LIST"

# update repo command
#curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && \
#    chmod a+x /usr/local/bin/repo


# add ssh keys for github and gitlab
mkdir -m u=rwx,o=,g= /root/.ssh
cat > /root/.ssh/known_hosts << EOF
|1|3mv1uZESpIgDMN2csWtt8/QA93Y=|5DWMb2RYILrrAVqC+iZVWoRY1Yg= ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl
|1|/0Ol6Bwlk2HuynpPxxLO1lmh3mY=|j9biaIDcnnzDucDSuFmwdzumnzA= ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf
EOF

# have to add my ssh pub key to get ssh access to github/gitlab
