#!/bin/bash

echo ">> [$(date)] Running userscript pre-build.sh before build"

# old kernel build, not needed anymore
# for device "cupid" only
#export TARGET_BOARD_PLATFORM="cupid"
#export OUT_DIR=${ANDROID_BUILD_TOP}/out/msm-kernel-${TARGET_BOARD_PLATFORM}
#
#export LTO=thin
#
#./kernel_platform/build/android/prepare_vendor.sh ${TARGET_BOARD_PLATFORM} gki
