# lineageos4microg_cupid


## Getting started

To make it easy for you to build LineageOS for Xiaomi 12 (cupid) this git contains local manifests and some scripts for usage with the [docker-lineage-cicd](https://github.com/lineageos4microg/docker-lineage-cicd) docker/podman container.
I will use podman but docker works the same way, only the command in the build script has to be changed.

First of all you have to create the container from here: [docker-lineage-cicd](https://github.com/lineageos4microg/docker-lineage-cicd)

If you use podman than use:

    podman build -t lineage-cicd https://github.com/lineageos4microg/docker-lineage-cicd.git

If you use docker than use:

    docker build -t lineage-cicd https://github.com/lineageos4microg/docker-lineage-cicd.git

Than edit the build script `build-cupid.sh` or use the podman/docker command directly on your favorite shell.
You have to change the variables:

* `DEVICE_LIST=cupid` to cupid (Xiaomi 12), zeus (Xiaomi 12 Pro) or mondrian (Poco F5 Pro)
* `WITH_GMS=true` to true if you want microG included or false for vanilla LineageOS.

Finally change all the paths `/mnt/build/lineageos4microG/...` to the path where you have put this repo. Keep in mind that you need a lot of space (~120GiB at least), use an SSD for faster build.

