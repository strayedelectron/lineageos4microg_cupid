#!/bin/bash

# Download current repopicks
wget -O userscripts/repopicks.sh https://gitlab.com/cupid-development/automated-builds/-/raw/main/repopicks.sh
chmod +x userscripts/repopicks.sh

# Run the container and start to build
podman run -it --rm -v "/mnt/build/lineageos4microG/lineage:/srv/src" -v "/mnt/build/lineageos4microG/zips:/srv/zips" -v "/mnt/build/lineageos4microG/logs:/srv/logs" -v "/mnt/build/lineageos4microG/cache:/srv/ccache" -v "/mnt/build/lineageos4microG/keys:/srv/keys" -v "/mnt/build/lineageos4microG/manifests:/srv/local_manifests" -v "/mnt/build/lineageos4microG/userscripts:/srv/userscripts" -e "ZIP_UP_IMAGES=true" -e "CALL_GIT_LFS_PULL=true" -e "RETRY_FETCHES=3" -e "SIGN_BUILDS=true" -e "WITH_GMS=true" -e "INCLUDE_PROPRIETARY=false" -e "BRANCH_NAME=lineage-21.0" -e "DEVICE_LIST=cupid" -e "PARALLEL_JOBS=10" -e "CALL_REPO-SYNC=true" -e "CLEAN_AFTER_BUILD=true" lineage-cicd


# add
#  -e "PARALLEL_JOBS=10"
# if you want to limit the number of parallel jobs
# 
# "INCLUDE_PROPRIETARY=false" because proprietary blobs are included in the manifest.
# "WITH_GMS=true" if you want to build with microG.
#
# For further instructions see: https://github.com/lineageos4microg/docker-lineage-cicd
